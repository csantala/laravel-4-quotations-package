<?php namespace Ablitica\Quotes;

class Quotes {
	
	function single()
	{
		$quotes = file_get_contents(realpath(dirname(__FILE__)).'/quotes.txt');
		preg_match_all('/"([^"]+)"/', $quotes, $quotesArray);
		$totalQuotes = count($quotesArray[0]) -1;
		return $quotesArray[1][rand(0, $totalQuotes)];
	}
}