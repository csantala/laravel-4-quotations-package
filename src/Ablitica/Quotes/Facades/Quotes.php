<?php namespace Ablitica\Quotes\Facades;
 
use Illuminate\Support\Facades\Facade;
 
class Quotes extends Facade {
 
  /**
   * Get the registered name of the component.
   *
   * @return string
   */
  protected static function getFacadeAccessor() { return 'quotes'; }
 
}