# Laravel 4 Quote Package #

Returns a single random quotation. 

Example: 
Management is doing things right; leadership is doing the right things. - Peter F. Drucker

## How to Install ##

In the ```require``` key of your ```composer.json``` file, add the following:

```"ablitica/quotes": "dev-master"```

Run the Composer update command:

```$ composer update```

In your ```config/app.php``` add ```'Ablitica\Quotes\QuotesServiceProvider'``` to the end of the ```$providers``` array.

## Usage ##

```
Route::get('/quote', function(){
	echo Quotes::single();
});
```